//
//  ViewController.swift
//  SceneKitPhysicsAnimTest
//
//  Created by Andrew Benson on 4/9/18.
//  Copyright © 2018 Nuclear Cyborg Corp. All rights reserved.
//

import UIKit
import SceneKit

class ViewController: UIViewController {

    @IBOutlet weak var sceneView: SCNView!
    let scene = SCNScene()
    
    
    // MARK: - CAMERA AND CAMERA ROTATION
    var camNode = SCNNode()
    private var rotate : Bool = false
    var rot : Float = 0 {
        didSet {
            updateCamPosition()
        }
    }
    
    @objc func doRotation(_ link:CADisplayLink) {
        guard rotate else { return }
        
        let RPS : Float = 0.25
        let actualFPS : Float = Float(1 / (link.targetTimestamp - link.timestamp))
        
        let adjustment : Float = Float(Float.pi * Float(2.0) * RPS / actualFPS)
        
        rot += adjustment
    }
    
    @IBOutlet weak var cameraSpinSwitch: UISwitch!
    @IBAction func cameraSpinSwitchValueChanged(_ sender: Any) {
        rotate = cameraSpinSwitch.isOn
    }
    
    
    // MARK: - CAMERA POSITIONING & ORIENTATION
    // Radius is camera distance from target node
    var targetNode : SCNNode!
    private var radius : Float = 2.2
    
    func updateCamPosition() {
        var x : Float = targetNode.position.x
        let y : Float = targetNode.position.y
        var z : Float = targetNode.position.z
        
        x += radius * sin(rot)
        z += radius * cos(rot)
        
        camNode.position = SCNVector3Make(x, y, z)
        camNode.look(at: targetNode.worldPosition, up: SCNVector3Make(0, 1, 0), localFront: SCNVector3Make(0, 0, -1))
    }
    
    
    // MARK: - GRAVITY CONTROL
    @IBOutlet weak var gravitySwitch: UISwitch!
    @IBAction func gravitySwitchValueChanged(_ sender: Any) {
        gravity = gravitySwitch.isOn
    }
    
    private var gravity : Bool = true {
        didSet {
            for n in blocks {
                n.physicsBody?.isAffectedByGravity = gravity
            }
        }
    }
    
    
    // MARK: - CONTROL OF LINKS BETWEEEN BLOCKS
    private enum LinkType : Int {
        case hinge = 0
        case ball  = 1
        case none  = 2
    }
    @IBOutlet weak var linkTypeControl: UISegmentedControl!
    
    @IBAction func linkTypeControlWasTapped(_ sender: Any) {
        
        scene.physicsWorld.removeAllBehaviors()
        
        switch linkTypeControl.selectedSegmentIndex {
        case LinkType.hinge.rawValue:
            linkBlocks(blocks, scene: scene, .hinge)
        case LinkType.ball.rawValue:
            linkBlocks(blocks, scene: scene, .ball)
        case LinkType.none.rawValue:
            break
        default:
            fatalError("linkTypeControlWasTapped with bad selectedSegmentIndex = \(linkTypeControl.selectedSegmentIndex)")
        }
        
    }

    
    // MARK: - BLOCKS SETUP AND LINKING
    var blocks: [SCNNode] = []

    private func createBlocks(count: Int) {
        for i in 0 ..< count {
            blocks.append( createBlock(heightIndex: i))
        }
    }
    private func createBlock(heightIndex: Int) -> SCNNode {
        let node = SCNNode()
        let geo = SCNBox(width: 0.20, height: 0.25, length: 0.10, chamferRadius: 0)

        var colors = [UIColor.green, // front
            UIColor.red, // right
            UIColor.blue, // back
            UIColor.yellow, // left
            UIColor.purple, // top
            UIColor.gray] // bottom
        if heightIndex == 0 {
            colors = [UIColor.red]
        }
        let sideMaterials = colors.map { color -> SCNMaterial in
            let material = SCNMaterial()
            material.diffuse.contents = color
            return material
        }

        geo.materials = sideMaterials
        let pShape = SCNPhysicsShape(geometry: geo)
        var pBody : SCNPhysicsBody
        if heightIndex == 0 {
            // The top block doesn't move
            pBody = SCNPhysicsBody(type: .kinematic, shape: pShape)
        } else {
            pBody = SCNPhysicsBody(type: .dynamic, shape: pShape)
        }
        pBody.allowsResting = true
        pBody.isAffectedByGravity = true
        pBody.damping = 0.5
        node.geometry = geo
        node.physicsBody = pBody
        
        node.position = SCNVector3Make(0, Float(1 - (CGFloat(heightIndex) * (geo.height + 0.02))), 0)
        
        return node
    }
    
    private func linkBlock(_ block: SCNNode, toBlock: SCNNode, scene: SCNScene, _ linkType: LinkType) {
        guard let block1body = block.physicsBody, let block2body = toBlock.physicsBody else {
            fatalError("can't link - no phsyics body")
        }
        let myAnchorPoint = getNodeBottom(block, offset: 0.05)
        let otherAnchorPoint = getNodeTop(toBlock)

        let myAxis = SCNVector3Make(1, 0, 0)
        let otherAxis = myAxis
    
        if linkType == .hinge {
            let joint = SCNPhysicsHingeJoint(bodyA: block1body, axisA: myAxis, anchorA: myAnchorPoint, bodyB: block2body, axisB: otherAxis, anchorB: otherAnchorPoint)
            scene.physicsWorld.addBehavior(joint)

        } else {
            let joint = SCNPhysicsBallSocketJoint(bodyA: block1body, anchorA: myAnchorPoint, bodyB: block2body, anchorB: otherAnchorPoint)
            scene.physicsWorld.addBehavior(joint)
        }
    }

    
    private func linkBlocks(_ blocks: [SCNNode], scene: SCNScene, _ linkType: LinkType ) {
        for i in 0 ..< blocks.count {
            if i > 0 {
                linkBlock(blocks[i], toBlock: blocks[i-1], scene: scene, linkType)
            }
        }
    }


    // MARK: - NODE POSITIONING HELPERS
    private func getNodeTop(_ node: SCNNode) -> SCNVector3 {
        return getNodeTop(node, offset: 0)
    }
    private func getNodeTop(_ node: SCNNode, offset : Float) -> SCNVector3 {
        
        
        let bb = node.boundingBox
        let x = (bb.min.x + bb.max.x) / 2
        let y = bb.min.y + offset
        let z = (bb.min.z + bb.max.z) / 2
        
        return SCNVector3Make(x, y, z)
        
    }
    
    private func getNodeBottom(_ node: SCNNode) -> SCNVector3 {
        return getNodeBottom(node, offset: 0)
    }
    private func getNodeBottom(_ node: SCNNode, offset: Float) -> SCNVector3 {
        
        
        let bb = node.boundingBox
        let x = (bb.min.x + bb.max.x) / 2
        let y = bb.max.y + offset
        let z = (bb.min.z + bb.max.z) / 2
        
        return SCNVector3Make(x, y, z)
        
    }
    
    private func loadOcto() -> SCNNode {
        return SCNNode()
    }
    

    // MARK: - HANDLE TAP & APPLY FORCE
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        let p = sender.location(in: sceneView)
        var optMap : [SCNHitTestOption : Any] = [:]
        optMap[.backFaceCulling] = true
        optMap[.firstFoundOnly] = true
        optMap[.sortResults] = true
        let results = sceneView.hitTest(p, options: optMap)
        for res in results {

            let power = Float(2.5)
            
            let vectorForce = SCNVector3Make( camNode.worldFront.x * power, camNode.worldFront.y * power, camNode.worldFront.z * power)

            res.node.physicsBody?.applyForce(vectorForce, at: res.localCoordinates, asImpulse: true)
        }
    }
    
    
    // MARK: - VIEW LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // FLOOR
        let floorNode = SCNNode()
        floorNode.geometry = SCNBox(width: 100, height: 1, length: 100, chamferRadius: 5)
        floorNode.position = SCNVector3Make(0, -2, 0)
        floorNode.physicsBody = SCNPhysicsBody(type: .kinematic, shape: SCNPhysicsShape(geometry: floorNode.geometry!, options: nil))
        let mat = SCNMaterial()
        mat.diffuse.contents = UIColor(red: 0.20, green: 0.40, blue: 0.80, alpha: 0.90)
        mat.specular.contents = UIColor.white
        mat.shininess = 1000.0
        
        floorNode.geometry!.materials = [mat]
        scene.rootNode.addChildNode(floorNode)
        
        // BLOCKS
        createBlocks(count: 8)
        linkBlocks(blocks, scene: scene, .hinge)
        for node in blocks {
            scene.rootNode.addChildNode(node)
        }
        targetNode = blocks[Int(blocks.count / 2)]
        
        
        // LIGHTS
        let omniLightNode = SCNNode()
        let light = SCNLight()
        light.type = .omni
        light.intensity = 500
        omniLightNode.light = light
        scene.rootNode.addChildNode(omniLightNode)
        
        let ambientLightNode = SCNNode()
        let light2 = SCNLight()
        light2.type = .ambient
        light2.intensity = 500
        light2.temperature = 5000
        ambientLightNode.light = light2
        scene.rootNode.addChildNode(ambientLightNode)
        
        let spotLightNode = SCNNode()
        let light3 = SCNLight()
        light3.type = .spot
        light3.intensity = 1000
        spotLightNode.light = light3
        spotLightNode.position = SCNVector3(0, 2, 1)
        spotLightNode.look(at: targetNode.worldPosition) // floorNode.worldPosition)
        scene.rootNode.addChildNode(spotLightNode)
        
        
//        camNode.look(at: spotLightNode.worldPosition, up: SCNVector(0, 1, 0), localFront: SCNVector(0,0,-1))
//        let constraint = SCNLookAtConstraint(target: ksdjfkds)
//        camNode.constraints = [constraint]
        
        
        // CAMERA
        camNode.position = SCNVector3Make(0, 0, 3.5)
        camNode.camera = SCNCamera()
        camNode.camera?.zNear = 0.01
        camNode.camera?.zFar = 1000
        scene.rootNode.addChildNode(camNode)
        
        sceneView.scene = scene
        sceneView.pointOfView = camNode
        sceneView.autoenablesDefaultLighting = false
        sceneView.allowsCameraControl = false
        
        // gestures
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tapGesture)
        
        // Spin camera
        rotate = false
        let link = CADisplayLink(target: self, selector: #selector(doRotation))
        link.preferredFramesPerSecond = 60
        link.add(to: .main, forMode: .defaultRunLoopMode)
        rot = 0
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

